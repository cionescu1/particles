/////////////////////////////////////////////////////////////////////////////////////////
//
// COMS20600 - WEEKS 6 and 7
// ASSIGNMENT 2
// CODE SKELETON
// TITLE: "LED Particle Simulation"
//
/////////////////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <platform.h>
#include <xccompat.h>
out port cled[4] = {PORT_CLOCKLED_0,PORT_CLOCKLED_1,PORT_CLOCKLED_2,PORT_CLOCKLED_3};
out port cledG = PORT_CLOCKLED_SELG;
out port cledR = PORT_CLOCKLED_SELR;
in port buttons = PORT_BUTTON;
out port speaker = PORT_SPEAKER;

#define noParticles 5 //overall number of particles threads in the system
#define startNoParticles 1 
int positions[5]  = {11, 1, 2 , 4, 8};
int directions[5] = { -1, 1, -1, 1, -1};


/////////////////////////////////////////////////////////////////////////////////////////
//
// Helper Functions provided for you
//
/////////////////////////////////////////////////////////////////////////////////////////


//DISPLAYS an LED pattern in one quadrant of the clock LEDs
void showLED(out port p, chanend fromVisualiser)
{
     unsigned int lightUpPattern;
     unsigned int running = 1;
     while (running == 1)
     {
         select
         {
             case fromVisualiser :> lightUpPattern: //read LED pattern from visualiser process
                 if (lightUpPattern == 100) //Signal to terminate
                 {
                     /* PUT HERE ANY LED FLASHES FOR AN END SEQUENCE*/
                     p <: 0;
                     running = 0;
                 }
                 else
                     p <: lightUpPattern; //send pattern to LEDs
                 break;
             default:
                 break;
         }
     }
     printf("showled stops\n");
}


//PLAYS a short sound (pls use with caution and consideration to other students in the labs!)
void playSound(unsigned int wavelength, int duration, out port speaker)
{
     timer tmr;
     int t, isOn = 1;
     tmr :> t;
     for (int i=0; i<duration; i++)
     {
         isOn = !isOn;
         t += wavelength;
         tmr when timerafter(t) :> void;
         speaker <: isOn;
     }
}


//WAIT function
void waitMoment(uint myTime)
{
     timer tmr;
     unsigned int waitTime;
     tmr :> waitTime;
     waitTime += myTime;
     tmr when timerafter(waitTime) :> void;
}


//READ BUTTONS and send commands to Visualiser
void buttonListener(in port buttons, chanend toVisualiser)
{
     int buttonInput; //button pattern currently pressed
     int firstTime = 0;
     unsigned int running = 1; //helper variable to determine system shutdown
     while (running == 1)
     {
         buttons when pinsneq(15) :> buttonInput;
         buttons when pinseq(15) :> void;
         waitMoment(8000000);
         if (buttonInput == 14)
         {
             toVisualiser <: 1; //start and pause simulation
             firstTime = 1;
         }
         if (buttonInput == 7)
         {
        	 if(firstTime == 1)
        	 {
				 running = 0;
				 toVisualiser <:0; //end simulation
           	 }
         }
         if (buttonInput == 13 && firstTime == 1)
             toVisualiser <:2; //Signal to decrease no of particles
        if (buttonInput == 11 && firstTime == 1)
             toVisualiser <:3; //Signal to increase no of particles
     }
     printf ("buttons stop\n");
}

//PROCESS TO COORDINATE DISPLAY of LED Particles
void visualiser(chanend toButtons, chanend show[], chanend toQuadrant[], out port speaker)
{
     unsigned int particles = startNoParticles;
     unsigned int display[noParticles]; //array of ant positions to be displayed, all values 0..11
     unsigned int running = 0; //helper variable to determine system shutdown
     int j; //helper variable
     int l; //helper variable
     int pause; //variable to pause simulation
     int timeToWait = 5000000;
     cledR <: 1;
     // the loading animation
     for(int i=0; i<=11; i++)
     {
        for(int p=0; p < 4; p++)
        {
             j = 0;
             for(int k=0; k<=i; k++)
             {
                j += (16<<(k%3))*(k/3==p);
             }
             toQuadrant[p] <: j;
             waitMoment(timeToWait);
        }
        waitMoment(timeToWait);
        toQuadrant[0] <: 0;
        toQuadrant[1] <: 0;
        toQuadrant[2] <: 0;
        toQuadrant[3] <: 0;
        waitMoment((12-i)*timeToWait);
     }
     // being ready to start the game
     toButtons :> running; //Wait for a button signal to initiate the simulation
     while (running == 1)
     {
         for (int k=0;k<noParticles;k++)
         {
             if (k%2 == 0)
            {
                 cledR <: 0;
                 cledG <: 1;
            }
             else
             {
                 cledG <: 0;
                 cledR <: 1;
             }

             select
             {
                 case toButtons :> running: //Receiving something from buttons
                     if (running == 0) //Signal to stop simulation
                      {
                          toQuadrant [0] <:100; //Switch off all LEDs
                          toQuadrant [1] <:100;
                          toQuadrant [2] <:100;
                          toQuadrant [3] <:100;
                          for (l = 0 ; l < noParticles; l++) //Stop all particle functions
                          {
                              show[l] :> j;
                              //printf ("received from %d particle\n", l);
                              show[l] <: 0;
                          }
                          break;
                      }
                     if (running == 1) //If signal is to pause simulation
                     {
                        while(1)
                        {
                            toButtons :> running; //wait for another button input
                            if (running == 0) //Signal to stop simulation
                            {
                              toQuadrant [0] <:100; //Switch off all LEDs
                              toQuadrant [1] <:100;
                              toQuadrant [2] <:100;
                              toQuadrant [3] <:100;
                              for (l = 0 ; l < noParticles; l++) //Stop all particle functions
                              {
                                  show[l] :> j;
                                  //printf ("received from %d particle\n", l);
                                  show[l] <: 0;
                              }
                              break;
                           }
                           if(running == 1)
                               break;
                           if(running == 2 && particles > 1)
                           {
                                running = 1;
                                particles --;
                                printf ("no of particles %d\n", particles);
                                for (l = 0 ; l < noParticles; l++) //Stop all particle functions
                                {
                                  show[l] :> j;
                                  show[l] <: 2;
                                }
                           }
                           if(running == 3 && particles < noParticles)
                           {
                                running = 1;
                                particles ++;
                                printf ("no of particles %d increased \n", particles);
                                for (l = 0 ; l < noParticles; l++) //Stop all particle functions
                                {
                                  show[l] :> j;
                                  show[l] <: 3;
                                }
                           }
                        }
                    }
                    else {
                        running = 1;
                        break;
                    }
                    break;

                 case show[k] :> j: //receiving from particle
                     if (running == 1)
                     {
                         show [k] <: 1;
                         if (j<12)
                             display[k] = j;
                         else
                             playSound(20000,20,speaker);
                     }
                 break;
                 default:
                     break;
             }

             if (running == 1)
             {
             //visualise particles
                 for (int i=0;i<4;i++)
                 {
                     j = 0;
                     for (int k=0;k<noParticles;k++)
                        j += (16<<(display[k]%3))*(display[k]/3==i);
                     toQuadrant[i] <: j;
                 }
             }
         }
     }
     printf ("Visualiser stops\n");
}

//PARTICLE...thread to represent a particle - to be replicated noParticle-times
void particle( streaming chanend left, streaming chanend right, chanend toVisualiser, int startPosition, int startDirection, int id)
{
     unsigned particles = startNoParticles;
     unsigned int moveCounter = 0; //overall no of moves performed by particle so far
     unsigned int position = startPosition; //the current particle position
     int attemptedPosition; //the next attempted position after considering move direction
     int currentDirection = startDirection; //the current direction the particle is moving
     int leftMoveForbidden = 0; //the verdict of the left neighbour if move is allowed
     int rightMoveForbidden = 0; //the verdict of the right neighbour if move is allowed
     int currentVelocity = 1; //the current particle velocity
     int running = 1;

     while (running == 1)
     {
         if (running != 0)
         {
             attemptedPosition = position + currentDirection; //Move to very next position
             if(attemptedPosition < 0) //Taking care of LEDs moving around the circle
                     attemptedPosition = 11;
             if(attemptedPosition > 11)
                     attemptedPosition = 0;



             if ( (id + 1) > particles)
             {
                // printf("I am hiding %d\n",id+1);
                attemptedPosition = 15;
             }

             right <: attemptedPosition; //Communicating with neighbours
             left :> leftMoveForbidden;
             left <: attemptedPosition;
             right :> rightMoveForbidden;



             //Preventing extra moves by faster particles which result in LED's switching off
             if(rightMoveForbidden != attemptedPosition && leftMoveForbidden != attemptedPosition) {
                  position = attemptedPosition;
             }
             else if (rightMoveForbidden == attemptedPosition)
             {
                 currentDirection = -currentDirection;
                 position = attemptedPosition;
             }
             else
                 currentDirection = -currentDirection;

             //Setting the speeds of particles
            waitMoment (40000000);



             // Showing wanted particles
             if ( (id+1) <= particles)
             {
                 toVisualiser <: position;
                 toVisualiser :> running;
                  if(running == 0)
                      running = 0;
                  if(running == 2)
                  {
                      particles --;
                      position = positions[id];
                      currentDirection = directions[id];
                      running = 1;
                  }
                  if(running == 3)
                  {
                      //printf("%d %d\n",id,particles);
                      particles ++;
                      position = positions[id];
                      currentDirection = directions[id];
                      running = 1;
                  }
             }
             //Hiding wanted particles
             if ( (id+1) > particles)
             {
                 toVisualiser <: -10;
                 toVisualiser :> running;
                 if(running == 0)
                      running = 0;
                  if(running == 2)
                  {
                      particles --;
                      position = positions[id];
                      currentDirection = directions[id];
                      running = 1;
                  }
                  if(running == 3)
                  {
                      particles ++;
                      position = positions[id];
                      currentDirection = directions[id];
                      running = 1;
                  }
                // printf("3 %d %d\n",id,particles);
             }
         }
     }
     printf ("Particle %d stops\n", id);
}

 //MAIN PROCESS defining channels, orchestrating and starting the threads
int main(void)
{
     chan quadrant[4]; //helper channels for LED visualisation
     chan show[noParticles]; //channels to link visualiser with particles
     streaming chan neighbours[noParticles]; //channels to link neighbouring particles
     chan buttonToVisualiser; //channel to link buttons and visualiser

 //MAIN PROCESS HARNESS
 par
 {

     //REPLICATING PARTICLES AND COMMUNICATION
     par (int i=0; i <= noParticles-1; i++)
     {
         on stdcore[(i+1)%4]: particle(neighbours[i], neighbours[(i+1)%(noParticles)], show[i], positions[i], directions[i], i);
     }

     //VISUALISER THREAD
     on stdcore[0]: visualiser(buttonToVisualiser,show,quadrant,speaker);
     //BUTTON LISTENER THREAD
     on stdcore[0]: buttonListener(buttons,buttonToVisualiser);

     //REPLICATION FOR THREADS PERFORMING LED VISUALISATION
     par (int k=0;k<4;k++)
     {
         on stdcore[k%4]: showLED(cled[k],quadrant[k]);
     }
 }
 return 0;
}
